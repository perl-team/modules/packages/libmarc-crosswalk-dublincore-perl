Source: libmarc-crosswalk-dublincore-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Danjean <vdanjean@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl,
               libmarc-record-perl,
               libdublincore-record-perl,
               libtest-pod-perl,
               libtest-pod-coverage-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmarc-crosswalk-dublincore-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmarc-crosswalk-dublincore-perl.git
Homepage: https://metacpan.org/release/MARC-Crosswalk-DublinCore

Package: libmarc-crosswalk-dublincore-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libmarc-record-perl,
         libdublincore-record-perl
Description: Convert data between MARC and Dublin Core
 MARC::Crosswalk::DublinCore provides an implementation of the LOC's spec
 on how to convert metadata between MARC and Dublin Core format. The spec
 for converting MARC to Dublin Core is available at:
 http://www.loc.gov/marc/marc2dc.html, and from DC to MARC:
 http://www.loc.gov/marc/dccross.html.
 .
 NB: The conversion cannot be done in a round-trip manner. i.e. Doing a
 conversion from MARC to DC, then trying to go back to MARC will not yield the
 original record.
